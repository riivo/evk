const fs = require('fs')
const request = require('superagent')

const { key } = require('./secrets')
const securities = require('./securities')

const date = new Date().toISOString().slice(0, 10)
let progress = 0

const getCaptchaId = security => {
  security.agent = request.agent()
  security.agent
    .get('https://nasdaqcsd.com/statistics/estonia/en/shareholders/security-info')
    .query({ isin: security.isin })
    .then(({ body: { data } }) => {
      security.captchaId = data.match(/src="\/statistics\/\/graphics\/captcha\/(.{32}).png"/)?.[1]
      if (!security.captchaId) {
        return console.error(security.name, 'no captcha ID found. Aborting')
      }
      console.log(security.name, 'got captcha ID', security.captchaId)
      getCaptchaImage(security)
    })
}

const getCaptchaImage = security => {
  security.agent
    .get(`https://nasdaqcsd.com/statistics/graphics/captcha/${security.captchaId}.png`)
    .then(({ header, body }) => {
      security.image = 'data:' + header['content-type'] +
        ';base64,' + new Buffer.from(body).toString('base64')
      getCaptchaInputRequest(security)
    })
}

const getCaptchaInputRequest = security => {
  request.post('https://2captcha.com/in.php')
    .send({
      key,
      method: 'base64',
      body: security.image,
      json: true
    })
    .then(({ text }) => {
      const { status, request } = JSON.parse(text)
      if (status !== 1) {
        console.error(security.name, '2Captcha request error:', text)
        if (request === 'ERROR_ZERO_BALANCE') return process.exit(1)
        return setTimeout(() => getCaptchaInputRequest(security), 5000)
      }
      security.captchaInputRequest = request
      getCaptchaInput(security)
    })
    .catch(({ message }) => {
      console.error(security.name, '2Captcha error:', message)
      setTimeout(() => getCaptchaInputRequest(security), 5000)
    })
}

const getCaptchaInput = security => {
  console.log(security.name, 'started 3 sec sleep')
  setTimeout(() => {
    request('https://2captcha.com/res.php')
      .query({
        key,
        action: 'get',
        id: security.captchaInputRequest,
        json: true
      })
      .then(({ body }) => {
        console.log(security.name, '2Captcha reply', body)
        const { request } = body
        if (request === 'CAPCHA_NOT_READY') return getCaptchaInput(security)
        else if (request === 'ERROR_CAPTCHA_UNSOLVABLE') return getCaptchaId(security)
        security.captchaInput = request
        getPDF(security)
      })
      .catch(({ message }) => {
        console.error(security.name, '2Captcha error:', message)
        setTimeout(() => getCaptchaInput(security), 5000)
      })
  }, 3000)
}

const getPDF = security => {
  const { agent, captchaId, captchaInput, isin, name } = security
  agent.get('https://nasdaqcsd.com/statistics/estonia/et/shareholders')
    .query({
      security: isin,
      'captcha[id]': captchaId,
      'captcha[input]': captchaInput
    })
    .responseType('blob')
    .then(({ body, status, type }) => {
      security.done = status === 200 && type === 'application/pdf'
      if (security.done) {
        fs.writeFileSync(`pdf-s/${date}_${isin}.pdf`, body)
        console.log(name, 'PDF size is', body.length, 'bytes |', ++progress, '/', securities.length)
        if (progress === securities.length) console.log('All done at', new Date().toLocaleString())
      }
      else {
        security.failedCount = (security.failedCount || 0) + 1
        console.error(name, 'PDF download failed at attempt', security.failedCount)
        if (security.failedCount === 5) {
          return console.error(name, '5 failures. Aborting')
        }
        setTimeout(() => getCaptchaId(security), 5000)
      }
    })
    .catch(({ message }) => {
      console.error(security.name, 'Nasdaq CSD error:', message)
      setTimeout(() => getCaptchaId(security), 5000)
    })
}

securities
  .forEach((security, i) => setTimeout(() => getCaptchaId(security), i * 3e3))