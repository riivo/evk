#!/bin/bash
ug -i --filter='pdf:pdftotext -layout % -' "$1" pdf-s/${2-$(ls pdf-s | tail -1 | cut -c1-10)}* | 
sed "$(jq -rj '.[] | "s/",.isin,"/",.name,"/;"' securities.json)"